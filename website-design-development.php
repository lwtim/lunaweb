<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Website design and development in Memphis");
	$smarty->assign("description","Website design & development, mobile web design, shopping cart design, website SEO and marketing. LunaWeb - Memphis web design since 1995.");
	$smarty->assign("keywords","website design memphis, web development memphis, web designers memphis, website designer in memphis tennessee, ecommerce designs memphis");
	$smarty->view("long_content.tpl");
?>