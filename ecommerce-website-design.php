<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Ecommerce website design and development in Memphis");
	$smarty->assign("description","Ecommerce webside design, mobile website design and website marketing services in Memphis. We combine design, PPC, SEO, web analytics and social media to show you how to profit from the web.");
	$smarty->assign("keywords","ecommerce web design memphis, ecommerce website design company in memphis tennessee, ppc ads, seo, inbound marketing");
	$smarty->view();
?>