<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Website marketing company in Memphis | Website design");
	$smarty->assign("description","Learn how our creative website design approach and website marketing process fosters both small and enterprise level businesses in Memphis & nationwide.");
	$smarty->assign("keywords","website marketing company memphis, website design mempis, internet marketing memphis");
	$smarty->view();
?>