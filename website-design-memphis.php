<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Website design Memphis | SEO Memphis | Internet Marketing");
	$smarty->assign("description","Memphis website design company since 1995. We offer unique web designs, ecommerce designs, web marketing, web strategies, SEO & mobile designs.");
	$smarty->assign("keywords","website design memphis, web design, web development, SEO memphis, website designers in memphis, tennessee");
	$smarty->view();
?>