<?php
	$redirects = array( //I don't have a list of redirects
	"/" => "/",
	"/Services/" => "/what-we-do.php",
	"/Services/Marketing/" => "/memphis-online-marketing.php",
	"/Services/Content/" => "/web-strategy-memphis.php",
	"/Services/SEO/" => "/memphis-seo",
	"/Services/SocialMedia/" => "/social-media-agency-memphis.php",
	"/Services/Consultation/" => "/internet-marketing-training.php",
	"/Services/Promotion/" => "/online-advertising.php",
	"/Services/PayPerClick/" => "/what-we-do-marketing-pay-per-click-advertising.php.php",
	"/Services/WebDevelopment/" => "/website-design-development.php",
	"/Portfolio/" => "/our-work.php",
	"/AboutUs/" => "/about-lunaweb.php",
	"/Contact/" => "/lets-talk.php",
	"/News/" => "/blog.php",
	"/blog" => "blog.lunaweb.com"
);

$filename = $_SERVER['REQUEST_URI'];

if(array_key_exists($filename,$redirects)) {
	header ('HTTP/1.1 301 Moved Permanently');
	header("Location: " . $redirects[$filename]);
} else {
	header ('HTTP/1.1 404 Not Found');
	echo file_get_contents('http://'.$_SERVER['SERVER_NAME'].'/404.php');
}
?>