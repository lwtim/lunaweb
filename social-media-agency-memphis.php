<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Social media agency in Memphis | Social media consulting");
	$smarty->assign("description","As the premiere social media agency in Memphis, LunaWeb teaches Memphis businesses how to profit from social media marketing. Call for social media consulting today!");
	$smarty->assign("keywords","social media agency memphis, social media consulting memphis, social media company memphis tennessee, lunaweb");
	$smarty->view();
?>