<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","PPC advertising - Memphis, Nationwide | Google ads, marketing campaigns");
	$smarty->assign("description","LunaWeb can create a PPC (pay-per-click) advertising campaign for you, show you how to maintain, and provide other online marketing services.");
	$smarty->assign("keywords","ppc advertising, ppc ads, google ads, web marketing, internet marketing memphis, pay per click ads, tennessee");
	$smarty->view();
?>