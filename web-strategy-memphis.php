<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Web strategy in Memphis | Internet Marketing - LunaWeb, Inc.");
	$smarty->assign("description","Web strategy development company in Memphis. Internet marketing, web optimization, PPC ads, Memphis SEO and analytics.");
	$smarty->assign("keywords","web strategy Memphis, online strategy, web optimization, internet marketing, memphis seo, ppc ads");
	$smarty->view();
?>