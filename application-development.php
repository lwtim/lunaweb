<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Memphis web application development");
	$smarty->assign("description","LunaWeb is the oldest web design and application development company in Memphis. Call us for expertise in website design, web marketing, SEO  and mobile web designs.");
	$smarty->assign("keywords","memphis application development, application development company, memphis, tennessee");
	$smarty->view();
?>