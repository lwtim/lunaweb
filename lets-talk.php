<?php

	if($_POST) {

	  $from_name = 'LunaWeb - Let\'s Talk';

		$from = 'info@lunaweb.com';
	 
		$to = 'sales@lunaweb.com';
		$subject = 'LunaWeb - Let\'s Talk';
		$semi_rand = md5(time());
		$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
		$headers = "From: ".$from .  "\r\nFrom: ".$from_name." <".$from.">\r\n";
		$headers .= "MIME-Version: 1.0\r\n" .
			"Content-Type: multipart/mixed;\r\n" .
			" boundary=\"{$mime_boundary}\"";
		$message_top = "This is a multi-part message in MIME format.\r\n" .
			"--{$mime_boundary}\r\n" .
			"Content-Type:text/html; charset=\"iso-8859-1\"\r\n" .
			"Content-Transfer-Encoding: 7bit\r\n";
			
			
			
		$message = "\n".'<table cellspacing="4">
		  <tr><td align="right">First Name : </td><td>'.$_POST['first_name'].'</td></tr>
		  <tr><td align="right">Last Name : </td><td>'.$_POST['last_name'].'</td></tr>
		  <tr><td align="right">Email : </td><td>'.$_POST['email'].'</td></tr>
		  <tr><td align="right">Phone Number : </td><td>'.$_POST['phone_number'].'</td></tr>
		  <tr><td align="right">Company Name : </td><td>'.$_POST['company_name'].'</td></tr>
		  <tr><td align="right">Website URL : </td><td>'.$_POST['website_url'].'</td></tr>
		  <tr><td align="right">Comments : </td><td>'.$_POST['comments'].'</td></tr>
		</table>';
		
		$message = stripslashes($message);
		$message = str_replace("\n","\r\n",$message);
		$message = $message_top . $message;

	  mail($to, $subject, $message, $headers);
	  echo '<script>window.location = "thank-you.php";</script>';
	}
	
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Contact LunaWeb - Memphis' oldest local web design firm");
	$smarty->assign("description","We specialize in creative web design, custom website development and online search marketing, including social media, SEO, PPC ads, etc. Unique websites to fit each individual client.");
	$smarty->assign("keywords","web design firm memphis, website designers in memphis, web consulting memphis, onine search marketing, social media, SEO, ppc ads");
	$smarty->view("long_content.tpl");
?>