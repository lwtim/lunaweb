			<div class="box">
				<h2>Newsletter</h2>
				<img src="images/newsletter-widget.jpg" alt="Newsletter signup for Memphis web design" class="image" /><br />
				<p>Go ahead – sign up for our monthly newsletter. We promise we'll only send useful tips and information to keep you up to date and savvier than your competitors, and we promise not to spam you or send you cat videos. </p>
				
<!-- newsletter  -->			
			<br />
			{include file='newsletter-form.tpl'}
			</div>