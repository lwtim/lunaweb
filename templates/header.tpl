<!-- start wrapper header -->
	<div id="{if $homepage=='true'}wrapper-header-01{else}wrapper-header-02{/if}">
		
		<div class="logo-01">
			<a href="/"><img src="design/logo-lunaweb-01.gif" alt="LunaWeb - Memphis web design company" /></a>
		</div>
		
		<div class="cta">		
			<a href="tel:9018885862">901-888-5862</a>
		</div>
		
		<div class="cta request-assessment">		
			<a href="lets-talk.php">Request an Assessment</a>
		</div>
		
		<div class="slogan">
			Designing great websites since 1995
		</div>
		
		{if $homepage=='true'}
		<div class="slider">
			<img src="design/slide-01.jpg" alt="LunaWeb portfolio of Memphis web designs" />
		</div>
		{/if}
		
	</div>
<!-- end wrapper header -->