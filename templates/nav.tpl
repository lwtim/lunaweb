<!-- start wrapper menu -->
	<div id="wrapper-menu-01">
		<div id="wrapper-menu-02">		
<!-- menu  -->			
			<div class="menu">
	
				<ul class="space-home"> 
					{if $homepage!="true"}                                     
					<li><a href="/">Home</a></li>
					{/if}
					<li>
						<a href="what-we-do{$php_extension}">What We Do</a>
						<ul>
							<li>
								<a href="memphis-online-marketing{$php_extension}">Marketing</a>
								<ul>
									<li><a href="web-strategy-memphis{$php_extension}">Strategy&nbsp;Development</a></li>
									<li><a href="memphis-inbound-marketing{$php_extension}">Inbound Marketing</a></li>
									<li><a href="memphis-seo{$php_extension}">Search&nbsp;Engine&nbsp;Optimization&nbsp;(SEO)</a></li>
									<li><a href="social-media-agency-memphis{$php_extension}">Social</a></li>
									<!--<li><a href="#">Mobile</a></li>-->
									<li><a href="internet-marketing-training{$php_extension}">Training&nbsp;and&nbsp;Consulting</a></li>
									<li><a href="online-advertising{$php_extension}">Online&nbsp;Advertising</a></li>
									<li><a href="what-we-do-marketing-pay-per-click-advertising{$php_extension}">Pay&#8209;per&#8209;click&nbsp;Ads&nbsp;(PPC)</a></li>
									<li><a href="email-marketing{$php_extension}">Email Marketing</a></li>
									<li><a href="website-analytics{$php_extension}">Website&nbsp;Analytics</a></li>
								</ul>
							</li>
							
							<li>
								<a href="website-design-development{$php_extension}">Development</a>
								<ul>
									<li><a href="website-design-Memphis{$php_extension}">Web&nbsp;Design</a></li>
									<li><a href="application-development{$php_extension}">Application</a></li>
									<li><a href="mobile-website-design{$php_extension}">Mobile&nbsp;Web</a></li>
									<li><a href="ecommerce-website-design{$php_extension}">Ecommerce</a></li>
								</ul>
							</li>
							
						</ul>
					</li>
					<li><a href="how-we-work{$php_extension}">How We Work</a></li>
					<li><a href="our-work{$php_extension}">Our Work</a></li>
					<li><a href="About-LunaWeb{$php_extension}">About Us</a></li>
					<li><a href="lets-talk{$php_extension}">Let’s Talk</a></li>
					<li><a href="http://blog.lunaweb.com">Blog</a></li>		
				</ul> 
			</div>
			
<!-- social  -->			
			<div class="social">			
				<a href="https://www.facebook.com/lunawebinc" target="_blank"><img src="design/icon-social-facebook-01.png" alt="" class="icon" /></a>
				<a href="http://www.youtube.com/user/Lunaweb1" target="_blank"><img src="design/icon-social-youtube-01.png" alt="" class="icon" /></a>
				<a href="https://twitter.com/lunaweb" target="_blank"><img src="design/icon-social-twitter-01.png" alt="" class="icon" /></a>
				<a href="http://www.linkedin.com/company/lunaweb?trk=top_nav_home" target="_blank"><img src="design/icon-social-linkedin-01.png" alt="" class="icon" /></a>
				<a href="http://blog.lunaweb.com/feed/rss/" target="_blank"><img src="design/icon-social-rss-01.png" alt="" class="icon" /></a>
				<a href="https://plus.google.com/104273967573924514221/posts" target="_blank"><img src="design/icon-social-gplus-01.png" alt="" class="icon" /></a>
			</div>	

		</div>						
	</div>
<!-- end wrapper menu -->