{* Smarty *}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		{include file='head.tpl'}
		<link rel="stylesheet" type="text/css" href="design/portfolio.css" /> 
		<link rel="stylesheet" type="text/css" href="design/parallax.css" /> 
		<script src="js/jQuery/jquery-ui-1.8.custom.min.js"></script>
		<script src="js/js.js"></script>
	</head>

	<body>

		
		<!-- ==================================================== START CONTENT ==================================================== -->
		
		<div id="wrapper-interior-01"><!-- start wrapper -->

		<!-- ==================================================== START HEADER ==================================================== -->
			{include file='header.tpl'}
		<!-- ==================================================== END HEADER ==================================================== -->


		<!-- ==================================================== START MENU ==================================================== -->
			{include file='nav.tpl'}
		<!-- ==================================================== END MENU ==================================================== -->





		<!-- ==================================================== START BODY ==================================================== -->

			{include file=$viewFile}	

		<!-- ==================================================== END BODY ==================================================== -->



		<!-- ==================================================== START FOOTER ==================================================== -->
			{include file='footer.tpl'}
		<!-- ==================================================== END FOOTER ==================================================== -->

		</div> <!-- end wrapper -->
	</body>
</html>
