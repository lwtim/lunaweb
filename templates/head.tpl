        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    
<!-- ==================================================== START SEO ==================================================== --> 


		<title>{$title}</title>
        <meta name="description" content="{$description}" />
        <meta name="keywords" content="{$keywords}" />
<!-- ==================================================== START COPYRIGHT ==================================================== -->
    
        <meta name="copyright" content="LunaWeb, Inc., All Rights Reserved." />
        <meta name="AUTHOR" content="LunaWeb, Inc." />
    
<!-- ==================================================== START ROBOTS ==================================================== -->    
    
        <meta name="Robots" content="index, follow, all" >
        <meta name="distribution" content="global" />
        <meta name="rating" content="general" />
        <meta name="resource-type" content="document" />
        <meta name="revisit-after" content="30 days" />
        <meta http-equiv="content-language" content="en-us" />
    
<!-- ==================================================== START CSS ==================================================== -->            
            
        <link rel="icon" href="favico.png" type="image/png" />
        
		<link rel="apple-touch-icon" href="apple-touch-icon-144x144.png" />
		<link rel="apple-touch-icon" href="apple-touch-icon-114x114.png" />
		<link rel="apple-touch-icon" href="apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon" href="apple-touch-icon.png" />
        
        <link href="design/responsive.css" rel="stylesheet" type="text/css" media="all" />      
    
<!-- ==================================================== START SCRIPT ==================================================== -->
		<script src="js/jQuery/jquery-1.10.2.min.js"></script>
		<script src="js/nav.js"></script>
		
<!-- ==================================================== START GOOGLE ANALYTICS ==================================================== -->

		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-684772-1']);
			//_gaq.push(['_setDomainName', 'lunaweb.net']);
			_gaq.push(['_setAllowLinker', true]);
			_gaq.push(['_trackPageview']);

			(function () {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>