			<div class="box">
				<h2>Interactive Expedition</h2>
				<img src="images/interactive-expedition.jpg" alt="Join Memphis internet meetup group - Interactive Expedition" class="image" /><br />
				<p>The <a href="http://interactiveexpedition.com/" target="_blank">Interactive Expedition</a> is an unofficial group of people interested in using, discovering, discussing, and sharing web technologies. <a href="http://www.meetup.com/socialexpedition/" target="_blank">Join us at our next event.</a></p>
				<p><br /><br /></p>
				<br />
				<a class="button" href="http://www.meetup.com/socialexpedition/" target="_blank"><span>Join Interactive Expedition now!</span></a>

			</div>