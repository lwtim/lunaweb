<!-- start wrapper-footer-01  -->	
	<div id="wrapper-footer-01">
		
		{if $homepage!="true"}<a href="/">Home</a>{/if}		
		<a href="what-we-do.php">What We Do</a>     
		<a href="how-we-work.php">How We Work</a>     
		<a href="our-work.php">Our  Work</a>     
		<a href="About-LunaWeb.php">About  Us</a>   
		<a href="lets-talk.php">Let’s Talk</a> 		
		<a href="http://blog.lunaweb.com">Blog</a>	
		<a href="sitemap.php">Sitemap</a>     
		<a href="privacy-policy.php">Privacy Policy</a>  
		
		<br /><br />
		
	<a href="tel:9018885862" class="no">901-888-5862</a> - <a href="mailto:info@lunaweb.net" class="no">info@lunaweb.net</a> - 5180 Park Ave, Memphis, TN 38119<br />
		Copyright &copy; <script>var currentTime = new Date();document.write(currentTime.getFullYear()); </script> LunaWeb, Inc.: A <a href="#" class="no">Memphis Web Design Company</a>, Memphis TN <br />
		All Rights Reserved. Terms of use. Your Privacy. <br />
		
	</div>
	
	<div id="wrapper-seo-01">
		
		LunaWeb is a Memphis web design company providing website marketing and web consulting. Please contact us for Internet services in Memphis, and nationwide:<br /><br />
		
		<span class="block">
			Memphis internet marketing<br /> 
			Inbound marketing company<br /> 
			PPC marketing<br />
			Social Media agency<br /> 
			Internet marketing campaigns<br />
		</span>
		
		<span class="block">
			Website design<br />
			Email marketing<br /> 
			Memphis SEO<br />
			Mobile marketing<br /> 
			Website analytics<br /> 
		</span>
		
		<span class="block">
			Mobile website design<br />
			CoreCommerce shopping carts<br /> 
			Website optimization<br /> 
			Neuromarketing agency<br />
			Web strategy development<br />
		</span>
		
	</div>
<!-- end wrapper-footer-01  -->	