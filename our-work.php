<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Memphis web design company | Website marketing");
	$smarty->assign("description","Memphis web design company portfolio - unique web design, ecommerce design, mobile website design, blogs and more. Website marketing specialists since 1995.");
	$smarty->assign("keywords","memphis web design company, website marketing memphis, seo memphis, internet marketing, memphis tennessee company");
	$smarty->view("portfolio.tpl");
?>