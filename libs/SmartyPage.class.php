<?php

	class SmartyPage extends Smarty
	{
		public function view($template="master.tpl"){
			$viewFile = 'views/'.substr(basename($_SERVER['PHP_SELF']),0,-3)."html";
			$this->assign('viewFile',$viewFile);

			// Only use .php extension on the test site
			if($_SERVER['SERVER_NAME']=="lunaweb.com")
				$this->assign("php_extension",".php");
			
			$this->display($template);
		}
	}

?>