<?php

	require_once( 'Smarty.class.php' );
	require_once( 'SmartyPage.class.php' );
	
	// NOTE: Smarty has a capital 'S'
	require_once('libs/Smarty.class.php');
	$smarty = new SmartyPage();

	$smarty->setTemplateDir('templates/');
	$smarty->setCompileDir('templates_c/');
	$smarty->setConfigDir('configs/');
	$smarty->setCacheDir('cache/');


?>