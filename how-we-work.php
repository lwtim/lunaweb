<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Memphis web strategy, PPC ads and analytics");
	$smarty->assign("description","Web strategy company in Memphis in 18th year. Experience in Internet marketing, web optimization, PPC ads, Memphis SEO, ecommerce web designs and website analytics.");
	$smarty->assign("keywords","memphis web strategy, ppc ads memphis tn tennessee, website analytics, ecommerce design, seo");
	$smarty->view();
?>