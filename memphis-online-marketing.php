<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Memphis online marketing firm - SEO, web optimization");
	$smarty->assign("description","Memphis online marketing marketing company offering website optimization, Memphis SEO, National SEO, PPC ads, web strategy and more.
");
	$smarty->assign("keywords","memphis online marketing firm, seo memphis, internet marketing in memphis, web marketing memphis, ppc ads, social media agency, memphis, tennessee
");
	$smarty->view();
?>