<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Memphis SEO | Internet marketing - LunaWeb, Inc.");
	$smarty->assign("description","Memphis SEO and internet marketing company with proven results in search engine optimization, PPC advertising, social media campaigns, and online reputation management.");
	$smarty->assign("keywords","memphis seo, memphis seo company, memphis search engine optimization, memphis tn seo, internet marketing memphis tn, memphis seo agency, memphis seo company");
	$smarty->view();
?>