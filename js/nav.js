$(document).ready(function(){
	
	$(".menu ul li").each(function(){
		$(this).has("ul").hover(
			function(){
				$(this).find("ul").first().slideDown(200);
			},
			function(){
				$(this).find("ul").first().hide();
			}
		);
	});
	
	$(".menu a").each(function(){
		if(this.href==window.location)
			$(this).parent().addClass("selected");
	});
	
	$(".menu ul li").has("ul").each(function(){
		if($(this).find("li.selected").length>0)
			$(this).addClass("selected");
	});
	
});
