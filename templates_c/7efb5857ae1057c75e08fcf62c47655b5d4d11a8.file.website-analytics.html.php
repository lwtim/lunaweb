<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:08:33
         compiled from "views\website-analytics.html" */ ?>
<?php /*%%SmartyHeaderCode:2376525c3efc561c69-65293467%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7efb5857ae1057c75e08fcf62c47655b5d4d11a8' => 
    array (
      0 => 'views\\website-analytics.html',
      1 => 1382129954,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2376525c3efc561c69-65293467',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c3efc625985_50781156',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c3efc625985_50781156')) {function content_525c3efc625985_50781156($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Website Analytics
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
	<!-- end chart  -->		
			<p> 
				<strong>Website Analytics - do you know how to read and monitor them?</strong>
				<br /><br />
				The web provides a wealth of analytics, but that data can be overwhelming. We work with you to help determine what numbers are meaningful and we help you set goals based on where you are. We look at things like the number of visitors, how many people converted on your website, and what pages people are visiting. We translate that into meaningful information that helps you make good business decisions.
				<br /><br />
				We can help you understand where you are, how specific campaigns are performing, and what to do next. <a href="/lets-talk.php">Contact us</a> to start tracking your progress!
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>