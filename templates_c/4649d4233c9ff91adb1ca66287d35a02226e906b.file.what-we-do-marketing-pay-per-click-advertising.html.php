<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:12:46
         compiled from "views\what-we-do-marketing-pay-per-click-advertising.html" */ ?>
<?php /*%%SmartyHeaderCode:19091525c1ea61f4a09-98558499%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4649d4233c9ff91adb1ca66287d35a02226e906b' => 
    array (
      0 => 'views\\what-we-do-marketing-pay-per-click-advertising.html',
      1 => 1382129954,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19091525c1ea61f4a09-98558499',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c1ea62b5190_03821068',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c1ea62b5190_03821068')) {function content_525c1ea62b5190_03821068($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Pay Per Click Advertising (PPC)
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
	<!-- end chart  -->		
			<p>
				Advertising is the time-honored method of getting the word out. LunaWeb knows what works, and we want to share that with you.
				We offer superior consulting for pay-per-click ad campaigns. There's no sleight-of-hand, only a powerful combination of targeting and strategy.
				<br /><br />
				We work with you to set measurable goals which we then monitor through the duration of the campaign. We monitor every campaign from day one, and stay hands-on throughout its duration. This is just part of LunaWeb's commitment to bringing you the best and most effective Internet marketing campaign possible.
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>