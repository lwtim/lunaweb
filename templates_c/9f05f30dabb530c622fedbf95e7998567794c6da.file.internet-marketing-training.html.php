<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:12:11
         compiled from "views\internet-marketing-training.html" */ ?>
<?php /*%%SmartyHeaderCode:6857525c21517e3b06-80729749%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9f05f30dabb530c622fedbf95e7998567794c6da' => 
    array (
      0 => 'views\\internet-marketing-training.html',
      1 => 1382129951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6857525c21517e3b06-80729749',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c21518a41a5_26793445',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c21518a41a5_26793445')) {function content_525c21518a41a5_26793445($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->

<div id="wrapper-body-01">
  <!-- start content -->
  <div id="content">
    <h1>Internet Marketing Training and Consulting</h1>
    <!-- start chart  -->
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
    <!-- end chart  -->
    <p><strong>Internet marketing training / Website application training</strong></p>
    <br />
    <p> We've enjoyed being an internet marketing and website design company in Memphis for a long time, and we're happy to share everything we've picked up along the way. We offer consulting and training on SEO, social media, web applications, and just about anything else web and marketing related.<br />
<br /></p>


<p>If you need assistance in developing, maintaining or executing your internet marketing campaign, <a href="/lets-talk.php">give us a call</a>. We love to share ideas!</p>
  </div>
  <!-- end content -->
</div>
<!-- end wrapper-body-01  -->
<div class="border-full-01">&nbsp;</div>
<br />
<br />
<!-- ==================================================== END BODY ==================================================== -->
<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
<div id="widgets">
  <!-- box 01  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  <!-- box 02  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  <!-- box 03  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<!-- end widgets  -->
<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>