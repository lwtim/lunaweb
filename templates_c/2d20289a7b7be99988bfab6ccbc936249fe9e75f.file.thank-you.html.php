<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 16:16:53
         compiled from "views\thank-you.html" */ ?>
<?php /*%%SmartyHeaderCode:15201525c53e2ae99b0-12545283%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2d20289a7b7be99988bfab6ccbc936249fe9e75f' => 
    array (
      0 => 'views\\thank-you.html',
      1 => 1382129953,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15201525c53e2ae99b0-12545283',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c53e2ba5809_75937205',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c53e2ba5809_75937205')) {function content_525c53e2ba5809_75937205($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Thank You
			</h1>
			<p>
				Thank you for contacting us. We'll be back in touch very soon. In the meantime, please visit our <a href="http://blog.lunaweb.com" target="_blank">blog</a> or connect with us on <a href="https://www.facebook.com/lunawebinc" target="_blank">Facebook</a> or <a href="https://twitter.com/lunaweb" target="_blank">Twitter</a>
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>