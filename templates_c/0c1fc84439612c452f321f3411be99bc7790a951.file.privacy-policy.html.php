<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:07:41
         compiled from "views\privacy-policy.html" */ ?>
<?php /*%%SmartyHeaderCode:11394526054b886e072-62057864%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0c1fc84439612c452f321f3411be99bc7790a951' => 
    array (
      0 => 'views\\privacy-policy.html',
      1 => 1382129953,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11394526054b886e072-62057864',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_526054b896bd62_19636682',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526054b896bd62_19636682')) {function content_526054b896bd62_19636682($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Privacy Policy
			</h1>

			<p>
				This Privacy Policy is subject to occasional amendment.
				<br /><br />
				LunaWeb, Inc. respects the privacy of visitors to our website.
				<br /><br />
				This privacy policy is intended to inform you of our policies regarding the collection, use and disclosure of any information you submit to us through our services. When you share information with us by using our services and accessing our website, we can make those services even better. By visiting our website and using our services, you agree to the terms of this Privacy Policy.
				<br /><br />
				We collect anonymous data to provide better services to all of our users. Information is collected through Google Analytics by use of a cookie that collects anonymous traffic data. Any information collected by us will not be used to track or collect personally identifiable information, nor will LunaWeb, Inc. share it with any third party other than Google. We may use this Anonymous Information to analyze usage patterns so that we may enhance our website, services and your user experience.
				<br /><br />
				Please be aware that by using our services, including our website, you consent to the terms of <a href="http://www.google.com/policies/privacy/">Google's Privacy Policy</a> as well as to ours. According to <a href="http://www.google.com/policies/privacy/">Google’s Privacy Policy</a>, Google will not share information about our site with any third parties unless Google (i) has our consent; (ii) concludes that it is required by law or has a good faith belief that access, preservation or disclosure of such information is reasonably necessary to protect the rights, property or safety of Google, its users or the public; or (iii) provides such information in certain limited circumstances to third parties to carry out tasks on Google's behalf (e.g., billing or data storage) with strict restrictions that prevent the data from being used or shared except as directed by Google.
				<br /><br />
				Collection of your information can be stopped by setting your browser to disable all cookies.
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>