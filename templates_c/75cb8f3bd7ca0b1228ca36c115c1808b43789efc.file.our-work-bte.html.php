<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:54:49
         compiled from "views\our-work-bte.html" */ ?>
<?php /*%%SmartyHeaderCode:2153752600e12de1c47-50299949%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '75cb8f3bd7ca0b1228ca36c115c1808b43789efc' => 
    array (
      0 => 'views\\our-work-bte.html',
      1 => 1382129952,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2153752600e12de1c47-50299949',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52600e12ea5f68_68628444',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52600e12ea5f68_68628444')) {function content_52600e12ea5f68_68628444($_smarty_tpl) {?><!-- ==================================================== PORTFOLIO ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
		<h1>
			Our Work
		</h1>
			<p>
				In the past 18 years of web design and development, we have seen the Internet grow, change, and progress in ways unimagined. LunaWeb continues to innovate as we provide a streamlined Web experience for our clients and their customers which meet or exceed the latest Web standards. Please take a look around and <a href="/lets-talk.php">get in touch</a>. We would love to talk to you about your needs.
			</p><br /><br />
<!-- viewer  -->		
		<div class="viewer">
			<div id="Parallax">
				<img src="images/BTERacing.png" />
				<img src="images/copyright-01.png" />
				<img src="images/copyright-02.png" />
			</div>
		</div>
<!-- copy  -->	
		<div class="copy">

			<h2>BTE Racing</h2><br /> 
			
			<p>
				BTE is a manufacturing, design, and support company that specializes in high performance automatic transmission assemblies and components for drag racing, off road, marine, and street performance. LunaWeb partners with BTE to bring more visitors to the website and increase sales through inbound marketing tactics. We consult, report, and discuss strategy with BTE on a regular basis.<br /><br />  
			</p>
			
			<h3>Services</h3> <br /> 
			
			<p>				
				Website Design<br> 
				Inbound Marketing and PPC<br> 
				Strategy Development and Email Marketing<br>
				Consulting and Reporting<br> 
				Search Engine Optimization<br> <br>  
			</p>	
			
			<h3>Website</h3> <br /> 
			
			<p>				
				<a href="http://bteracing.com/" target="_blank">http://bteracing.com/</a>	<br /> <br /> <br /> 
				
				<a class="button" href="/our-work-ll-nursing.php"><span>Previous</span></a><a class="button" href="/our-work-mcc.php"><span>Next</span></a>
			</p>

		</div>

	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END PORTFOLIO ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 02  -->			
			<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 03  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>