<?php /* Smarty version Smarty-3.1.13, created on 2013-10-21 08:24:16
         compiled from "templates\nav.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14285252cde3535de8-46754526%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e72a3457a1d204c1558ea437888c1e0adefb627b' => 
    array (
      0 => 'templates\\nav.tpl',
      1 => 1382365454,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14285252cde3535de8-46754526',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5252cde35526e8_08654877',
  'variables' => 
  array (
    'homepage' => 0,
    'php_extension' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5252cde35526e8_08654877')) {function content_5252cde35526e8_08654877($_smarty_tpl) {?><!-- start wrapper menu -->
	<div id="wrapper-menu-01">
		<div id="wrapper-menu-02">		
<!-- menu  -->			
			<div class="menu">
	
				<ul class="space-home"> 
					<?php if ($_smarty_tpl->tpl_vars['homepage']->value!="true"){?>                                     
					<li><a href="/">Home</a></li>
					<?php }?>
					<li>
						<a href="what-we-do<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">What We Do</a>
						<ul>
							<li>
								<a href="memphis-online-marketing<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Marketing</a>
								<ul>
									<li><a href="web-strategy-memphis<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Strategy&nbsp;Development</a></li>
									<li><a href="memphis-inbound-marketing<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Inbound Marketing</a></li>
									<li><a href="memphis-seo<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Search&nbsp;Engine&nbsp;Optimization&nbsp;(SEO)</a></li>
									<li><a href="social-media-agency-memphis<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Social</a></li>
									<!--<li><a href="#">Mobile</a></li>-->
									<li><a href="internet-marketing-training<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Training&nbsp;and&nbsp;Consulting</a></li>
									<li><a href="online-advertising<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Online&nbsp;Advertising</a></li>
									<li><a href="what-we-do-marketing-pay-per-click-advertising<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Pay&#8209;per&#8209;click&nbsp;Ads&nbsp;(PPC)</a></li>
									<li><a href="email-marketing<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Email Marketing</a></li>
									<li><a href="website-analytics<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Website&nbsp;Analytics</a></li>
								</ul>
							</li>
							
							<li>
								<a href="website-design-development<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Development</a>
								<ul>
									<li><a href="website-design-Memphis<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Web&nbsp;Design</a></li>
									<li><a href="application-development<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Application</a></li>
									<li><a href="mobile-website-design<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Mobile&nbsp;Web</a></li>
									<li><a href="ecommerce-website-design<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Ecommerce</a></li>
								</ul>
							</li>
							
						</ul>
					</li>
					<li><a href="how-we-work<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">How We Work</a></li>
					<li><a href="our-work<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Our Work</a></li>
					<li><a href="About-LunaWeb<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">About Us</a></li>
					<li><a href="lets-talk<?php echo $_smarty_tpl->tpl_vars['php_extension']->value;?>
">Let’s Talk</a></li>
					<li><a href="http://blog.lunaweb.com">Blog</a></li>		
				</ul> 
			</div>
			
<!-- social  -->			
			<div class="social">			
				<a href="https://www.facebook.com/lunawebinc" target="_blank"><img src="design/icon-social-facebook-01.png" alt="" class="icon" /></a>
				<a href="http://www.youtube.com/user/Lunaweb1" target="_blank"><img src="design/icon-social-youtube-01.png" alt="" class="icon" /></a>
				<a href="https://twitter.com/lunaweb" target="_blank"><img src="design/icon-social-twitter-01.png" alt="" class="icon" /></a>
				<a href="http://www.linkedin.com/company/lunaweb?trk=top_nav_home" target="_blank"><img src="design/icon-social-linkedin-01.png" alt="" class="icon" /></a>
				<a href="http://blog.lunaweb.com/feed/rss/" target="_blank"><img src="design/icon-social-rss-01.png" alt="" class="icon" /></a>
				<a href="https://plus.google.com/104273967573924514221/posts" target="_blank"><img src="design/icon-social-gplus-01.png" alt="" class="icon" /></a>
			</div>	

		</div>						
	</div>
<!-- end wrapper menu --><?php }} ?>