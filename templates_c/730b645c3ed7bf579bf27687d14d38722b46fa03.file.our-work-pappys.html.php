<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:26:08
         compiled from "views\our-work-pappys.html" */ ?>
<?php /*%%SmartyHeaderCode:18379526005800501e4-90219798%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '730b645c3ed7bf579bf27687d14d38722b46fa03' => 
    array (
      0 => 'views\\our-work-pappys.html',
      1 => 1382129953,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18379526005800501e4-90219798',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52600580133686_07101553',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52600580133686_07101553')) {function content_52600580133686_07101553($_smarty_tpl) {?><!-- ==================================================== PORTFOLIO ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
		<h1>
			Our Work
		</h1>
			<p>
				In the past 18 years of web design and development, we have seen the Internet grow, change, and progress in ways unimagined. LunaWeb continues to innovate as we provide a streamlined Web experience for our clients and their customers which meet or exceed the latest Web standards. Please take a look around and <a href="/lets-talk.php">get in touch</a>. We would love to talk to you about your needs.
			</p><br /><br />
<!-- viewer  -->		
		<div class="viewer">
			<div id="Parallax">
				<img src="images/PappysCoffee.png" />
				<img src="images/copyright-01.png" />
				<img src="images/copyright-02.png" />
			</div>
		</div>
<!-- copy  -->	
		<div class="copy">

			<h2>Pappy's Coffee</h2><br /> 
			
			<p>
				Pappy's Coffee brings delicious coffee and excellent customer service to Memphis. LunaWeb and Pappy's worked together to design and develop a website that tells the story of Pappy's and features available coffee products and equipment. <br /><br />  
			</p>
			
			<h3>Services</h3> <br /> 
			
			<p>				
				Website Design<br /> <br /> 
			</p>	
			
			<h3>Website</h3> <br /> 
			
			<p>				
				<a href="http://pappyscoffee.com/" target="_blank">http://pappyscoffee.com/</a>	<br /> <br /> <br /> 
				
				<a class="button" href="/our-work-ll-anesthesia.php"><span>Previous</span></a><a class="button" href="/our-work-gates.php"><span>Next</span></a>
			</p>

		</div>

	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END PORTFOLIO ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 02  -->			
			<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 03  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>