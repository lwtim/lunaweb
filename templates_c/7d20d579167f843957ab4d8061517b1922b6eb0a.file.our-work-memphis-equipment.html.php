<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:26:09
         compiled from "views\our-work-memphis-equipment.html" */ ?>
<?php /*%%SmartyHeaderCode:524352600e318104b8-49515118%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7d20d579167f843957ab4d8061517b1922b6eb0a' => 
    array (
      0 => 'views\\our-work-memphis-equipment.html',
      1 => 1382129953,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '524352600e318104b8-49515118',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52600e318d2c08_37481105',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52600e318d2c08_37481105')) {function content_52600e318d2c08_37481105($_smarty_tpl) {?><!-- ==================================================== PORTFOLIO ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
		<h1>
			Our Work
		</h1>
			<p>
				In the past 18 years of web design and development, we have seen the Internet grow, change, and progress in ways unimagined. LunaWeb continues to innovate as we provide a streamlined Web experience for our clients and their customers which or exceed the latest Web standards. Please take a look around and <a href="/lets-talk.php">get in touch</a>. We would love to talk to you about your needs.
			</p><br /><br />
<!-- viewer  -->		
		<div class="viewer">
			<div id="Parallax">
				<img src="images/MemphisEquipmentCompany.png" />
				<img src="images/copyright-01.png" />
				<img src="images/copyright-02.png" />
			</div>
		</div>
<!-- copy  -->	
		<div class="copy">

			<h2>Memphis Equipment Company</h2><br /> 
			
			<p>
				Memphis Equipment Company specializes in U.S. military 4x4 and 6x6 trucks and parts. LunaWeb partners with Memphis Equipment to optimize sales and leads through the website by implementing clear calls-to-action, valuable offers, strategic email marketing, as well as search engine optimization and pay-per-click ads. <br /><br />  
			</p>
			
			<h3>Services</h3> <br /> 
			
			<p>				
				Website Design<br /> 
				Inbound Marketing and PPC<br />
				Strategy Development and Email Marketing<br />
				Consulting and Reporting<br />
				Search Engine Optimization<br /> <br /> 
			</p>	
			
			<h3>Website</h3> <br /> 
			
			<p>				
				<a href="http://memphisequipment.com/" target="_blank">http://memphisequipment.com/</a>	<br /> <br /> <br /> 
				
				<a class="button" href="/our-work-railking.php"><span>Previous</span></a><a class="button" href="/our-work-bluesky.php"><span>Next</span></a>
			</p>

		</div>

	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END PORTFOLIO ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 02  -->			
			<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 03  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>