<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:10:32
         compiled from "views\application-development.html" */ ?>
<?php /*%%SmartyHeaderCode:13574525c43b58ba0b3-25005454%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d2940ed177760f746fa86b71215e665acb98421' => 
    array (
      0 => 'views\\application-development.html',
      1 => 1382129951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13574525c43b58ba0b3-25005454',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c43b5978e78_87746700',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c43b5978e78_87746700')) {function content_525c43b5978e78_87746700($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Application Development
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
	<!-- end chart  -->		
			<p>
				Need custom programming that goes beyond a standard website? Our clients' needs vary, which is why we offer custom application development. Whether you need an Intranet or you have an idea for an enterprise application, we can help. <a href="/lets-talk.php">Just ask us</a>.
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>