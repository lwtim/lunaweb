<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:08:58
         compiled from "views\online-advertising.html" */ ?>
<?php /*%%SmartyHeaderCode:21966525c21a16bb750-10203274%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'abcec3895f7dcaa7d0b6574ab42ac6438414b81f' => 
    array (
      0 => 'views\\online-advertising.html',
      1 => 1382129952,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21966525c21a16bb750-10203274',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c21a177ce43_94552260',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c21a177ce43_94552260')) {function content_525c21a177ce43_94552260($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Online Advertising
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
	<!-- end chart  -->		
			<p>
				Reach exactly who you want through effective advertising. Online campaigns can focus on your target audience in a way traditional media campaigns simply can't. 
				<br /><br />
				LunaWeb will leverage creative and engaging campaigns to produce measurable, defined results through a variety of online marketing channels.
				<br /><br />
				We offer Internet marketing services that will draw eyes to your site, event coverage and promotion, and all the content and creative services you'll need throughout the campaign.
				<br /><br />
				After your campaign's completion, we'll provide you with information about the return on your investment.
				<br /><br />
				No matter what tools you use to promote your brand, normal Internet marketing or social media, our campaign and promotion services promise to deliver serious, concrete results. 
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>