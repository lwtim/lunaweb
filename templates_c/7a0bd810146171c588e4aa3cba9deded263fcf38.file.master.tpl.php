<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 16:08:01
         compiled from "templates\master.tpl" */ ?>
<?php /*%%SmartyHeaderCode:156175252cde335af90-03098530%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7a0bd810146171c588e4aa3cba9deded263fcf38' => 
    array (
      0 => 'templates\\master.tpl',
      1 => 1382129944,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '156175252cde335af90-03098530',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5252cde3438374_49882744',
  'variables' => 
  array (
    'homepage' => 0,
    'viewFile' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5252cde3438374_49882744')) {function content_5252cde3438374_49882744($_smarty_tpl) {?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"> 

	<head>
		<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	</head>

	<body>

		
		<!-- start wrapper -->
		<div id="<?php if ($_smarty_tpl->tpl_vars['homepage']->value=='true'){?>wrapper-home-01<?php }else{ ?>wrapper-interior-01<?php }?>">
				
		<!-- ==================================================== HEADER ==================================================== -->
			<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<!-- ==================================================== END HEADER ==================================================== -->


		<!-- ==================================================== START MENU ==================================================== -->
			<?php echo $_smarty_tpl->getSubTemplate ('nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<!-- ==================================================== END MENU ==================================================== -->





		<!-- ==================================================== START BODY ==================================================== -->

			<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['viewFile']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	

		<!-- ==================================================== END BODY ==================================================== -->



		<!-- ==================================================== START FOOTER ==================================================== -->
			<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<!-- ==================================================== END FOOTER ==================================================== -->

		</div>
		<!-- end wrapper -->
	</body>
</html>
<?php }} ?>