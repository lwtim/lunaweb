<?php /* Smarty version Smarty-3.1.13, created on 2013-10-21 08:44:18
         compiled from "templates\header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:137995252cde3505ed2-42430601%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '20a5b87bf1d249a8e4b5bdf6dc560aa9c65c681a' => 
    array (
      0 => 'templates\\header.tpl',
      1 => 1382366655,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '137995252cde3505ed2-42430601',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5252cde350a7e2_93186952',
  'variables' => 
  array (
    'homepage' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5252cde350a7e2_93186952')) {function content_5252cde350a7e2_93186952($_smarty_tpl) {?><!-- start wrapper header -->
	<div id="<?php if ($_smarty_tpl->tpl_vars['homepage']->value=='true'){?>wrapper-header-01<?php }else{ ?>wrapper-header-02<?php }?>">
		
		<div class="logo-01">
			<a href="/"><img src="design/logo-lunaweb-01.gif" alt="LunaWeb - Memphis web design company" /></a>
		</div>
		
		<div class="cta">		
			<a href="tel:9018885862">901-888-5862</a>
		</div>
		
		<div class="cta request-assessment">		
			<a href="lets-talk.php">Request an Assessment</a>
		</div>
		
		<div class="slogan">
			Designing great websites since 1995
		</div>
		
		<?php if ($_smarty_tpl->tpl_vars['homepage']->value=='true'){?>
		<div class="slider">
			<img src="design/slide-01.jpg" alt="LunaWeb portfolio of Memphis web designs" />
		</div>
		<?php }?>
		
	</div>
<!-- end wrapper header --><?php }} ?>