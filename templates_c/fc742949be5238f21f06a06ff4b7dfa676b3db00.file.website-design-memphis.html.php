<?php /* Smarty version Smarty-3.1.13, created on 2013-10-14 16:02:03
         compiled from "views\website-design-memphis.html" */ ?>
<?php /*%%SmartyHeaderCode:31624525c5bcb66b3e9-20126535%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fc742949be5238f21f06a06ff4b7dfa676b3db00' => 
    array (
      0 => 'views\\website-design-memphis.html',
      1 => 1381779639,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '31624525c5bcb66b3e9-20126535',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c5bcb731cf5_10624599',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c5bcb731cf5_10624599')) {function content_525c5bcb731cf5_10624599($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Web Design
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01">
				
				<span class="title">Lorem ipsum dolo sit amet conscte tuer adipiscing</span><br /><br />
				<span class="copy">Nonum my <a href="#">euismod</a> nibh! </span><br />		
				<span class="list">
					design<br />	
					coding <br />	
					marketing<br />	
				</span>
			</div>
	<!-- end chart  -->		
			<p>
				Web design is more than just colors and fonts, which is why we embrace a holistic view of design that encompasses more than just what you see - it's about the whole visitor experience. Our designers will match form and function to create the innovative, professional, and effective website you're looking for. Schedule a free consultation.
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>