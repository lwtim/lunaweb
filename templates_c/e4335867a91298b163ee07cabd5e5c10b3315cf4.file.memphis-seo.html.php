<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 19:33:58
         compiled from "views\memphis-seo.html" */ ?>
<?php /*%%SmartyHeaderCode:30889525c224569a077-41352309%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e4335867a91298b163ee07cabd5e5c10b3315cf4' => 
    array (
      0 => 'views\\memphis-seo.html',
      1 => 1382129952,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '30889525c224569a077-41352309',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c22457612d1_68276406',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c22457612d1_68276406')) {function content_525c22457612d1_68276406($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Search Engine Optimization
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
	<!-- end chart  -->		
			<p>
				What happens if people can't find your website when they search for keywords that apply to your business? You lose business. If your website is not on the first or second page of search results, you're probably losing customers. LunaWeb can improve your website performance in the search engines.
				<br /><br />
				<strong>What is SEO?</strong>
				<br /><br />
				Search Engine Optimization (SEO) is the process of utilizing techniques to get your website found in the organic (free) listings of search engine result pages. We utilize 'white hat' SEO techniques so you are assured we only employ ethical search engine practices that help people to find your website. The techniques are used on your web pages  and on other content such as videos, images, blogs, articles, social media, and local and national business listing pages that are considered relevant to users by the search engines. Our team is focused on helping you get more visitors to your website.
				<br /><br />
				<strong>Local and National Search Engine Optimization (SEO)</strong>
				<br /><br />
				If you offer products or services mainly to the local Memphis area, you would be interested in local Memphis SEO.  Examples of local businesses are dry cleaners, restaurants, plumbers, computer repair, etc.
				<br /><br />
				National SEO is aimed at businesses who offer their products and services to a specific geographic area of the U.S., or if products and services are offered nationwide.
				<br /><br />
				If you want your website to appear quickly in the top results of the search engines or want to promote something special, we can help you with Pay Per Click <a href="/what-we-do-marketing-pay-per-click-advertising.php">(PPC)</a> advertising which can get you on the first page of the search results by tomorrow. 
				<br /><br />
				<a href="/lets-talk.php">Talk to one of our marketing experts to learn more about Memphis SEO or national SEO.</a>
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>