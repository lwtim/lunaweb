<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 16:26:46
         compiled from "views\our-work.html" */ ?>
<?php /*%%SmartyHeaderCode:23823525c49bb482371-54696993%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f5207dd4c531779264ee7b3762cf6ef9bc88a45a' => 
    array (
      0 => 'views\\our-work.html',
      1 => 1382129953,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '23823525c49bb482371-54696993',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c49bb54bcf9_12430227',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c49bb54bcf9_12430227')) {function content_525c49bb54bcf9_12430227($_smarty_tpl) {?><!-- ==================================================== PORTFOLIO ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
		<h1>
			Our Work
		</h1>
			<p>
				In the past 18 years of web design and development, we have seen the Internet grow, change, and progress in ways unimagined. LunaWeb continues to innovate as we provide a streamlined Web experience for our clients and their customers which meet or exceed the latest Web standards. Please take a look around and <a href="/lets-talk.php">get in touch</a>. We would love to talk to you about your needs.
			</p><br /><br />
<!-- viewer  -->		
		<div class="viewer">
			<div id="Parallax">
				<img src="images/portfolio-wmt-conserve-plus-v01.jpg" />
				<img src="images/copyright-01.png" />
				<img src="images/copyright-02.png" />
			</div>
		</div>
<!-- copy  -->	
		<div class="copy">

			<h2>CONSERVE® PLUS</h2>
			<h3> Wright Medical Technology, Inc.</h3><br /> 
			
			<p>
				Wright Medical Technology, Inc. is an ISO 9001 certified designer, manufacturing and distributing orthopaedic implants and instrumentation worldwide for more than 60 years. Wright is committed to finding solutions to the challenges that face today's orthopaedic professionals. Through the website, users can browse information about corporate compliance, reimbursement, and prescribing, as well as find a physician. <br /><br />  
			</p>
			
			<h3>Services</h3> <br /> 
			
			<p>				
				Website Design<br />
				Content Management System<br />
				Search Engine Optimization<br /> <br />
			</p>	
			
			<h3>Website</h3> <br /> 
			
			<p>				
				<a href="http://www.wmt.com/conserveplus" target="_blank">www.wmt.com/conserveplus</a>	<br /> <br /> <br /> 
				
				<a class="button" href="/our-work-bluesky.php"><span>Previous</span></a><a class="button" href="/our-work-ll-anesthesia.php"><span>Next</span></a>
			</p>

		</div>

	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END PORTFOLIO ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 02  -->			
			<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 03  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>