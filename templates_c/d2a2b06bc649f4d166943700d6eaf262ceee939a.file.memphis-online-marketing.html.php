<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 19:18:56
         compiled from "views\memphis-online-marketing.html" */ ?>
<?php /*%%SmartyHeaderCode:30987525c25c1b2bf73-96438905%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd2a2b06bc649f4d166943700d6eaf262ceee939a' => 
    array (
      0 => 'views\\memphis-online-marketing.html',
      1 => 1382129952,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '30987525c25c1b2bf73-96438905',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c25c1bef854_88809760',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c25c1bef854_88809760')) {function content_525c25c1bef854_88809760($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->

<div id="wrapper-body-01">
  <!-- start content -->
  <div id="content">
    <h1>Marketing</h1>
    <!-- start chart  -->
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
    <!-- end chart  -->
    <p>Having a website is no longer enough to grow your business. There was a time when you could build a website and people might show up without a lot of effort on your part. But those days are a memory. Those days are a memory – the reality is that if you want to grow your business, you have to actively work at getting more people to your site and converting them to leads and customers. This needs to happen strategically and this is where we can help you by doing the following:</p>
    <br />

    <ul>
      <li>Develop an effective marketing strategy based on your goals</li>
      <li>Create meaningful content people will read</li>
      <li>Get more visitors to your website</li>
      <li>Convert more website visitors into leads and buying customers</li>
      <li>Build repeat business</li>
      <li>Connect with customers online</li>
      <li>Measure the results of your marketing</li>
      <li>Get a clear understanding of your online marketing ROI<br />
      </li>
    </ul>
 <p><br />
LunaWeb is a Memphis based internet marketing company that has worked with hundreds of clients – get in touch with us for an assessment of your online marketing. We would love to talk to you and see if we can help you.</p>
    
  </div>
  <!-- end content -->
</div>
<!-- end wrapper-body-01  -->
<div class="border-full-01">&nbsp;</div>
<br />
<br />
<!-- ==================================================== END BODY ==================================================== -->
<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
<div id="widgets">
  <!-- box 01  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  <!-- box 02  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  <!-- box 03  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<!-- end widgets  -->
<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>