<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:29:21
         compiled from "views\our-work-gates.html" */ ?>
<?php /*%%SmartyHeaderCode:554352600dd8663f64-13250577%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ef3e1f70c5231797afcee3038bef61995f59f82c' => 
    array (
      0 => 'views\\our-work-gates.html',
      1 => 1382129952,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '554352600dd8663f64-13250577',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52600dd87264b8_96965199',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52600dd87264b8_96965199')) {function content_52600dd87264b8_96965199($_smarty_tpl) {?><!-- ==================================================== PORTFOLIO ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
		<h1>
			Our Work
		</h1>
			<p>
				In the past 18 years of web design and development, we have seen the Internet grow, change, and progress in ways unimagined. LunaWeb continues to innovate as we provide a streamlined Web experience for our clients and their customers which meet or exceed the latest Web standards. Please take a look around and <a href="/lets-talk.php">get in touch</a>. We would love to talk to you about your needs.
			</p><br /><br />
<!-- viewer  -->		
		<div class="viewer">
			<div id="Parallax">
				<img src="images/GatesLumber.png" />
				<img src="images/copyright-01.png" />
				<img src="images/copyright-02.png" />
			</div>
		</div>
<!-- copy  -->	
		<div class="copy">

			<h2>Gates Lumber</h2><br /> 
			
			<p>
				Gates Lumber is a family-owned custom millwork company that specializes in all types of stock, custom millwork and wood products. LunaWeb worked to highlight customer testimonials on the website while featuring an extensive line of products. <br /><br />
			</p>
			
			<h3>Services</h3> <br /> 
			
			<p>				
				Web design<br /> <br /> 
			</p>	
			
			<h3>Website</h3> <br /> 
			
			<p>				
				<a href="http://gateslumber.com/" target="_blank">http://gateslumber.com/</a>	<br /> <br /> <br /> 
				
				<a class="button" href="/our-work-pappys.php"><span>Previous</span></a><a class="button" href="/our-work-piper.php"><span>Next</span></a>
			</p>

		</div>

	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END PORTFOLIO ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 02  -->			
			<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 03  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>