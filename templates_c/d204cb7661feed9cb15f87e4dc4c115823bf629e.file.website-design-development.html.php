<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:11:52
         compiled from "views\website-design-development.html" */ ?>
<?php /*%%SmartyHeaderCode:15056525c1f39b84027-60327756%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd204cb7661feed9cb15f87e4dc4c115823bf629e' => 
    array (
      0 => 'views\\website-design-development.html',
      1 => 1382129954,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15056525c1f39b84027-60327756',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c1f39c56c79_98673372',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c1f39c56c79_98673372')) {function content_525c1f39c56c79_98673372($_smarty_tpl) {?><!-- ==================================================== GENERAL ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
	<!-- header-image  -->	
			<div class="header-image">
				<img src="images/header-design-development.jpg" alt="" />
			</div>
	<!-- title  -->			
			<h1>
				Design and Development
			</h1>
	<!-- copy  -->	
			<p>
				<strong>Development Services</strong>
				<br /><br />
				We have developed a lot of websites since 1995, and we can tell you firsthand that <strong>a lot</strong> has changed on the web and things continue to change very quickly. We stay on top of the newest advancements in <a href="/website-design-Memphis.php">design</a>, <a href="/application-development.php">programming</a>, and <a href="/mobile-website-design.php">mobile solutions</a> while ensuring we meet the latest in W3 web standards to deliver you and your customers the best user experience possible.
				<br/ ><br />
				Our process is designed to make the process as easy as possible and to produce a website that looks great and achieves your business goals. We can get your website or application up and running swiftly and smoothly.
				<br/ ><br />
				Our development services include a custom design based on your requirements and goals and conclude with a finished website or web application. We can work with you to develop an online store, static websites, mobile friendly sites, or custom web applications tht support and connect to other web systems. Our work is designed and built with care and precision. <a href="lets-talk.php">Get in touch</a> to find out what we can develop for you.
				<br/ ><br />
				<strong>Web Design</strong>
				<br/ ><br />
				Web design is more than just colors and fonts, which is why we embrace a holistic view of design that encompasses more than just what you see &mdash; it's about the whole experience. Our designers will match form and function to create the innovative, professional, and effective website you're looking for. <a href="lets-talk.php">Schedule a free consultation</a>.
				<br/ ><br />
				<strong>Application</strong>
				<br/ ><br />
				Need custom programming that goes beyond a standard website? Our clients' needs vary, which is why we offer custom application development. Whether you need an Intranet or you have an idea for an enterprise application, we can help. <a href="lets-talk.php">Just ask us</a>.
				<br/ ><br />
				<strong>Mobile Web</strong>
				<br/ ><br />
				As early proponents of the mobile web, we know how important it is for your business's website to be tablet and smartphone-ready, which is why we offer both responsive designs and standalone mobile websites to fit the needs of our clients. Don't know which one you need? We're standing by with the answers you need &mdash; just give us a call!
				<br/ ><br />
				<strong>Ecommerce</strong>
				<br/ ><br />
				When your online store is your bread and butter, we know you want to put it in good hands. We've got the experience you're looking for, no matter how large or small your store.
				<br/ ><br />
				Not only can we build your store to your specifications, but we can also offer database management and ongoing support, so you'll have us when you need us. <a href="/lets-talk.php">Call us</a> to talk about your online store today!<br /><br />
			</p>			
		</div>
<!-- end content -->
<!-- start widgets  -->	
	<div id="widgets">
		
<!-- facebook  -->	
			<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook_stream.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
<!-- cta    -->			
							
			
<!-- green-box  -->				
			<?php echo $_smarty_tpl->getSubTemplate ('widget_download_guide.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		
	</div>			
<!-- end widgets  -->	

	



	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div>

<!-- ==================================================== END GENERAL ==================================================== --><?php }} ?>