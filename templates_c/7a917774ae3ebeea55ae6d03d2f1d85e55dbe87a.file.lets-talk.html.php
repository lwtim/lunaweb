<?php /* Smarty version Smarty-3.1.13, created on 2013-10-21 08:39:25
         compiled from "views\lets-talk.html" */ ?>
<?php /*%%SmartyHeaderCode:20646525c4131ce7ad2-42416232%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7a917774ae3ebeea55ae6d03d2f1d85e55dbe87a' => 
    array (
      0 => 'views\\lets-talk.html',
      1 => 1382366363,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20646525c4131ce7ad2-42416232',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c4131dac766_58399308',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c4131dac766_58399308')) {function content_525c4131dac766_58399308($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Let's Talk
			</h1>
	<!-- end chart  -->		
			<p>
				Give us a call or contact us through the form below. We want to learn more about you, your company, and your goals so we can see if we can help you achieve your goals. Feel free to stop by the office to grab a cup of coffee &mdash; we're in East Memphis. We'd love to chat.

				Our phone number is <a href="tel:9018885862">(901)&nbsp;888&#8209;5862</a>. You can also connect with us on <a href="https://www.facebook.com/lunawebinc" target="_blank">Facebook</a> or <a href="https://twitter.com/lunaweb" target="_blank">Twitter</a>. We look forward to talking soon.
			</p>
			
			<form id="lets-talk" action="lets-talk.php" method="post">
				<label for="first_name">First Name<sup class="required">*</sup> : </label><input type="text" name="first_name" />
				<label for="last_name">Last Name<sup class="required">*</sup> : </label><input type="text" name="last_name" />
				<label for="email">Email<sup class="required">*</sup> : </label><input type="text" name="email" />
				<label for="phone_number">Phone Number : </label><input type="phone" name="phone_number" />
				<label for="company_name">Company Name : </label><input type="text" name="company_name" />
				<label for="website_url">Website URL : </label><input type="text" name="website_url" />
				<label for="comments">Comments : <br /><span>Tell Us How We Can Help</span></label><textarea name="comments"></textarea>
				<div class="form-bottom">
					<div class="button"><span>Submit</span></div>
					<br />
					<br />
					<br />
					<sup>*</sup>Required fields
				</div>
			</form>
			
			<script type="text/javascript" src="js/jQuery/jquery.validate.min.js"></script>
			<script type="text/javascript" src="js/jQuery/additional-methods.min.js"></script>
			<script>
				$("#lets-talk").validate({
					rules: {
						first_name: {
						  required: true
						},
						last_name: {
						  required: true
						},
						phone_number: {
						  phoneUS: true
						},
						email: {
						  required: true,
						  email: true
						}
					 }
				});
				
				$(document).ready(function(){
					$("#lets-talk .button").click(function(){
						$("#lets-talk").submit();
					});
				});
			</script>
			
		</div>
<!-- end content -->

<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- facebook  -->	
			<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook_stream.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
<!-- cta    -->			
							
			
<!-- green-box  -->				
			<?php echo $_smarty_tpl->getSubTemplate ('widget_download_guide.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
<!-- ==================================================== END CTA ==================================================== -->

	</div>
<!-- end wrapper-body-01  -->	

<!-- ==================================================== END BODY ==================================================== -->
<?php }} ?>