<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:15:11
         compiled from "views\memphis-inbound-marketing.html" */ ?>
<?php /*%%SmartyHeaderCode:7492525c2120d45287-14305189%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4839fb029dfa9a82c1cc45145764f899c23aaae1' => 
    array (
      0 => 'views\\memphis-inbound-marketing.html',
      1 => 1382129952,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7492525c2120d45287-14305189',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c2120e079e2_78375762',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c2120e079e2_78375762')) {function content_525c2120e079e2_78375762($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Inbound Marketing
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
	<!-- end chart  -->		
			<p>
				Traditional, outbound marketing is dead. Well, it's not dead, but it's not as effective as it used to be and it's more expensive on a cost-per-lead basis than inbound marketing . So what is Inbound Marketing? It's the process of getting people to your website when they  are interested in what you offer vs. outbound marketing which pushes a message and hopes to catch people at the right time (e.g., television commercials, radio ads, billboards, direct mail, etc.).  
				<br /><br />
				By leveraging activities such as blogging and social media, your business can draw in new customers by offering them quality and educational content. We'll help you get found, convert your readers to customers, and measure your success. It's a holistic marketing strategy that will change the whole way you think about getting your message out there. Want to learn more? <a href="/lets-talk.php">Schedule a free consultation</a>!
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>