<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:11:14
         compiled from "views\ecommerce-website-design.html" */ ?>
<?php /*%%SmartyHeaderCode:5089525c3fa1def3e5-28848243%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8c0a6e5176ec34cdad7a51f4710528cc6d1e7259' => 
    array (
      0 => 'views\\ecommerce-website-design.html',
      1 => 1382129951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5089525c3fa1def3e5-28848243',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c3fa1eae636_35833840',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c3fa1eae636_35833840')) {function content_525c3fa1eae636_35833840($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Ecommerce Websites
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
	<!-- end chart  -->		
			<p>
				When your online store is your bread and butter, we know you want to put it in good hands. We have the experience you're looking for, no matter how large or small your store.
				<br /><br />
				Not only can we build your store to your specifications, but we can also offer database management and ongoing support, so you'll have us when you need us. Call us to talk about your ecommerce website today!
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>