<?php /* Smarty version Smarty-3.1.13, created on 2013-10-14 10:32:58
         compiled from "views\interior-portfolio.html" */ ?>
<?php /*%%SmartyHeaderCode:98835258655cc1cb19-90617298%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dd9b3caa402cafb33674179221e6aace9823108a' => 
    array (
      0 => 'views\\interior-portfolio.html',
      1 => 1381762951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '98835258655cc1cb19-90617298',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5258655cce8553_73461274',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5258655cce8553_73461274')) {function content_5258655cce8553_73461274($_smarty_tpl) {?><!-- ==================================================== PORTFOLIO ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
		<h1>
			Portfolio
		</h1>
		
<!-- viewer  -->		
		<div class="viewer">
			<div id="Parallax">
				<img src="images/portfolio-wmt-conserve-plus-v01.jpg" />
				<img src="images/copyright-01.png" />
				<img src="images/copyright-02.png" />
			</div>
		</div>
<!-- copy  -->	
		<div class="copy">

			<h2>CONSERVE® PLUS</h2>
			<h3> Wright Medical Technology, Inc.</h3><br /> 
			
			<p>
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diamon non ummy nibh euismod tincidunt ut laoreet dolore magna 
				aliqupta amerat olutpat. Ut wisi enim ad minim veniam, quis nostrud exerci an tation ullamcor per suscipit lobortis nisl ut ..<br /><br />  
			</p>
			
			<h3>Services</h3> <br /> 
			
			<p>				
				Web design<br /> 
				Consequat vel illum dolore<br /> 
				Eu feugiat nulla facilisis at<br /> 
				Wero eros et accumsan et iusto <br /> 
				Search Engine Optimization<br /> <br /> 
			</p>	
			
			<h3>Website</h3> <br /> 
			
			<p>				
				<a href="http://www.wmt.com/conserveplus" target="_blank">www.wmt.com/conserveplus</a>	<br /> <br /> <br /> 
				
				<a class="button" href="???"><span>Previous</span></a><a class="button" href="???"><span>Next</span></a>
			</p>

		</div>

	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END PORTFOLIO ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 02  -->			
			<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 03  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_contact.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>