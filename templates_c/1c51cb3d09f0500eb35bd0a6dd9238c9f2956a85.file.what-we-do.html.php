<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 16:14:16
         compiled from "views\what-we-do.html" */ ?>
<?php /*%%SmartyHeaderCode:15731525c2059a5a287-41567967%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1c51cb3d09f0500eb35bd0a6dd9238c9f2956a85' => 
    array (
      0 => 'views\\what-we-do.html',
      1 => 1382129954,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15731525c2059a5a287-41567967',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c2059b1e769_04647298',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c2059b1e769_04647298')) {function content_525c2059b1e769_04647298($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				What We Do
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
	<!-- end chart  -->		
			<p>
				"What does LunaWeb do?" 
				<br /><br />
				We get asked that a lot. In short, we help business owners, management, and marketers get results through their website. Results depend on your goals, timeline, budget, audience, and what you've already done (or haven't done). We take care of the details so you can concentrate on doing what you do best.
				<br /><br />
				Here's a quick list of what we do for our clients:
				<br /><br />
				<ul>
					<li>Design and develop websites that look great and accomplish a business need.</li>
					<li>Develop effective marketing strategies targeted to your audiences.</li>
					<li>Increase your website traffic though search engine optimization (SEO), pay per click (PPC) advertising.</li>
					<li>Create great content for your blog and website.</li>
					<li>Engage your audiences through social media channels such as Twitter, LinkedIn, and Facebook.</li>
					<li>Inform customers and stay top of mind through email marketing campaigns designed to educate your audiences and draw them back to your website.</li>
					<li>Convert website visitors to leads (and ultimately to customers) through enticing offers (such as e-books), landing pages, calls-to-action (CTAs).</li>
					<li>Lead nurturing email campaigns.</li>
					<li>Measure the results and ROI which we report monthly.</li>
					<li>Provide ongoing support for your website through website hosting and website maintenance.</li>
				</ul>
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>