<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 20:54:55
         compiled from "views\about-lunaweb.html" */ ?>
<?php /*%%SmartyHeaderCode:6087525c454c7b32d2-10688900%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7c2020206505b34508f430b06fdbee0c64f20014' => 
    array (
      0 => 'views\\about-lunaweb.html',
      1 => 1382129951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6087525c454c7b32d2-10688900',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c454c8786c4_99966442',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c454c8786c4_99966442')) {function content_525c454c8786c4_99966442($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				About Us
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
	<!-- end chart  -->		
			<p>
				When LunaWeb started in 1995, the Web looked a lot different than it does today. Back then, we taught clients how to use a mouse and explained the practical and beneficial uses of email. Now we educate our clients on how to use websites to generate more leads, create better customer experiences, and create operating efficiencies.
				<br /><br />
				Our team has more than 60 years of real-world business experience, not only in web design and development, but also running businesses that grow. We use our business experience and our web know-how to help you develop a solid, goal-oriented strategy to grow your business through the web.
				<br /><br />
				We get to know your company, your brand, and your goals to make solid recommendations that can impact your bottom line. Our approach results in long-lasting relationships with our clients &mdash; an average of more than eight years.
				<br /><br />
				Take a look around and <a href="/lets-talk.php">get in touch</a> when you're ready to learn more.
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>