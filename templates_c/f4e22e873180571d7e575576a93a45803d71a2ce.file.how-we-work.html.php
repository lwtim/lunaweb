<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 16:09:10
         compiled from "views\how-we-work.html" */ ?>
<?php /*%%SmartyHeaderCode:22669525c45f1a35e57-68175238%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f4e22e873180571d7e575576a93a45803d71a2ce' => 
    array (
      0 => 'views\\how-we-work.html',
      1 => 1382129951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22669525c45f1a35e57-68175238',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c45f1af79a4_60368068',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c45f1af79a4_60368068')) {function content_525c45f1af79a4_60368068($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				How We Work
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
	<!-- end chart  -->		
			<p>
				First, we ask a lot of questions. It's important to determine if LunaWeb is a good fit for you and your business needs. As we take our business relationships seriously, we view the time invested on the front end as investment in a mutually profitable partnership.
				<br /><br />
				Then, after we've met a few times, we'll put together a proposal with recommendations that are tailored to your business goals.
				<br /><br />
				Once you approve the proposal, we will begin to implement and execute through completion. Regular communication and collaboration as we move forward will help us know your business better and serve you more effectively.
				<br /><br />
				<a href="/lets-talk.php">Contact us</a> to get the process started – or if you just want to hear our great phone voices.
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>