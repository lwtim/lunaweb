<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:13:08
         compiled from "views\our-work-bluesky.html" */ ?>
<?php /*%%SmartyHeaderCode:2524052600e3ac743b7-07911369%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a904c97a8c8fd888cc3c70c3b232de04fb3a850b' => 
    array (
      0 => 'views\\our-work-bluesky.html',
      1 => 1382129952,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2524052600e3ac743b7-07911369',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52600e3ad361b6_64214970',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52600e3ad361b6_64214970')) {function content_52600e3ad361b6_64214970($_smarty_tpl) {?><!-- ==================================================== PORTFOLIO ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
		<h1>
			Portfolio
		</h1>
			<p>
				In the past 18 years of web design and development, we have seen the Internet grow, change, and progress in ways unimagined. LunaWeb continues to innovate as we provide a streamlined Web experience for our clients and their customers which meet or exceed the latest Web standards. Please take a look around and <a href="/lets-talk.php">get in touch</a>. We would love to talk to you about your needs.
			</p><br /><br />
<!-- viewer  -->		
		<div class="viewer">
			<div id="Parallax">
				<img src="images/BlueSkyCouriers.png" />
				<img src="images/copyright-01.png" />
				<img src="images/copyright-02.png" />
			</div>
		</div>
<!-- copy  -->	
		<div class="copy">

			<h2>BlueSkyCouriers</h2><br /> 
			
			<p>
				Blue Sky Couriers is a Memphis-based immediate package delivery company voted "Best Courier in Memphis" by readers of the Memphis Business Journal. LunaWeb worked with Blue Sky to design and develop a website that gives basic information about the company and allows customers to submit online orders.<br /><br />  
			</p>
			
			<h3>Services</h3> <br /> 
			
			<p>				
				Website Design<br /> 
				Search Engine Optimization<br /> <br />
			</p>	
			
			<h3>Website</h3> <br /> 
			
			<p>				
				<a href="http://www.blueskycouriers.com/" target="_blank">http://www.blueskycouriers.com/</a>	<br /> <br /> <br /> 
				
				<a class="button" href="/our-work-memphis-equipment.php"><span>Previous</span></a><a class="button" href="/our-work.php"><span>Next</span></a>
			</p>

		</div>

	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END PORTFOLIO ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 02  -->			
			<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 03  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>