<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 20:33:13
         compiled from "views\social-media-agency-memphis.html" */ ?>
<?php /*%%SmartyHeaderCode:29681525c1d1ee067d1-26522166%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1e8ecc563d67d2dcbddc1ed79144601ab48f989e' => 
    array (
      0 => 'views\\social-media-agency-memphis.html',
      1 => 1382129953,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29681525c1d1ee067d1-26522166',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c1d1eec7f25_43954369',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c1d1eec7f25_43954369')) {function content_525c1d1eec7f25_43954369($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->

<div id="wrapper-body-01">
  <!-- start content -->
  <div id="content">
    <h1> Social Media Marketing</h1>
    <!-- start chart  -->
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
    <!-- end chart  -->
    <p><strong>Are you connecting with your customers and prospects beyond your website?</strong></p>
    <br />
    <p> Sixty-seven percent of Americans are using social media (<a href="http://pewinternet.org/Commentary/2012/March/Pew-Internet-Social-Networking-full-detail.aspx" target="_blank">Pew Internet Research</a>) so your customers are most likely using social media. Whether it's Facebook, LinkedIn, Twitter, or Pinterest, we can help you figure out the best way to engage in the conversation and build a better relationship with your customers.<br />
    </p><br />

    <p>As a social media agency in Memphis, LunaWeb can go beyond setting up your social media accounts. We can also: <br />
      <br />
    <ul>
      <li> Train your company's social media point person</li>
      <li>Assist you in updating your social media networks regularly</li>
      <li>Optimize your social media accounts for the search engines; and</li>
      <li>Provide social media consulting</li>
    </ul>
    </p>
    <p> <br />
      <a href="lets-talk.php">Call LunaWeb</a> for social media consulting today - in the greater Memphis area, or nationwide.</p>
  </div>
  <!-- end content -->
</div>
<!-- end wrapper-body-01  -->
<div class="border-full-01">&nbsp;</div>
<br />
<br />
<!-- ==================================================== END BODY ==================================================== -->
<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
<div id="widgets">
  <!-- box 01  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  <!-- box 02  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  <!-- box 03  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<!-- end widgets  -->
<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>