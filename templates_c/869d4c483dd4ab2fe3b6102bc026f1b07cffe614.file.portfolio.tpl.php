<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 16:26:46
         compiled from "templates\portfolio.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8960525868a7986d96-22400408%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '869d4c483dd4ab2fe3b6102bc026f1b07cffe614' => 
    array (
      0 => 'templates\\portfolio.tpl',
      1 => 1382129944,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8960525868a7986d96-22400408',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525868a7a65383_31437149',
  'variables' => 
  array (
    'viewFile' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525868a7a65383_31437149')) {function content_525868a7a65383_31437149($_smarty_tpl) {?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<link rel="stylesheet" type="text/css" href="design/portfolio.css" /> 
		<link rel="stylesheet" type="text/css" href="design/parallax.css" /> 
		<script src="js/jQuery/jquery-ui-1.8.custom.min.js"></script>
		<script src="js/js.js"></script>
	</head>

	<body>

		
		<!-- ==================================================== START CONTENT ==================================================== -->
		
		<div id="wrapper-interior-01"><!-- start wrapper -->

		<!-- ==================================================== START HEADER ==================================================== -->
			<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<!-- ==================================================== END HEADER ==================================================== -->


		<!-- ==================================================== START MENU ==================================================== -->
			<?php echo $_smarty_tpl->getSubTemplate ('nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<!-- ==================================================== END MENU ==================================================== -->





		<!-- ==================================================== START BODY ==================================================== -->

			<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['viewFile']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	

		<!-- ==================================================== END BODY ==================================================== -->



		<!-- ==================================================== START FOOTER ==================================================== -->
			<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<!-- ==================================================== END FOOTER ==================================================== -->

		</div> <!-- end wrapper -->
	</body>
</html>
<?php }} ?>