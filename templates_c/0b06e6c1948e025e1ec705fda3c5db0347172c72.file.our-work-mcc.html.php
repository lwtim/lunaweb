<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:44:07
         compiled from "views\our-work-mcc.html" */ ?>
<?php /*%%SmartyHeaderCode:1902852600e1ff36989-20448369%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b06e6c1948e025e1ec705fda3c5db0347172c72' => 
    array (
      0 => 'views\\our-work-mcc.html',
      1 => 1382129953,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1902852600e1ff36989-20448369',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52600e200b8cb8_81472155',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52600e200b8cb8_81472155')) {function content_52600e200b8cb8_81472155($_smarty_tpl) {?><!-- ==================================================== PORTFOLIO ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
		<h1>
			Our Work
		</h1>
			<p>
				In the past 18 years of web design and development, we have seen the Internet grow, change, and progress in ways unimagined. LunaWeb continues to innovate as we provide a streamlined Web experience for our clients and their customers which meet or exceed the latest Web standards. Please take a look around and <a href="/lets-talk.php">get in touch</a>. We would love to talk to you about your needs.
			</p><br /><br />
<!-- viewer  -->		
		<div class="viewer">
			<div id="Parallax">
				<img src="images/MemphisChildrensClinic.png" />
				<img src="images/copyright-01.png" />
				<img src="images/copyright-02.png" />
			</div>
		</div>
<!-- copy  -->	
		<div class="copy">

			<h2>Memphis Children's Clinic</h2><br /> 
			
			<p>
				Memphis Children's Clinic provides excellent medical care for children through Knowledge, Quality, Compassion, and Continuity. Through a playful and colorful design, the website gives basic information about the clinic and its doctors.<br /><br />  
			</p>
			
			<h3>Services</h3> <br /> 
			
			<p>				
				Website Design<br /> 
				Voice of the Patient<br /> <br /> 
			</p>	
			
			<h3>Website</h3> <br /> 
			
			<p>				
				<a href="http://www.memphischildrensclinic.org/" target="_blank">http://www.memphischildrensclinic.org/</a>	<br /> <br /> <br /> 
				
				<a class="button" href="/our-work-bte.php"><span>Previous</span></a><a class="button" href="/our-work-railking.php"><span>Next</span></a>
			</p>

		</div>

	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END PORTFOLIO ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 02  -->			
			<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
			
<!-- box 03  -->
			<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	
	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>