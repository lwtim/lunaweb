<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 16:09:49
         compiled from "templates\long_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2522252530a64599116-06061504%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0d1a44e6ab789dfc9273abe9860150d37fe61c30' => 
    array (
      0 => 'templates\\long_content.tpl',
      1 => 1382129943,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2522252530a64599116-06061504',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52530a6466fb88_90621248',
  'variables' => 
  array (
    'viewFile' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52530a6466fb88_90621248')) {function content_52530a6466fb88_90621248($_smarty_tpl) {?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<link href="design/long_content.css" rel="stylesheet" type="text/css" media="all" />  
	</head>

	<body>

		
		<!-- ==================================================== START CONTENT ==================================================== -->
		
		<div id="wrapper-interior-01"><!-- start wrapper -->

		<!-- ==================================================== START HEADER ==================================================== -->
			<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<!-- ==================================================== END HEADER ==================================================== -->


		<!-- ==================================================== START MENU ==================================================== -->
			<?php echo $_smarty_tpl->getSubTemplate ('nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<!-- ==================================================== END MENU ==================================================== -->





		<!-- ==================================================== START BODY ==================================================== -->

			<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['viewFile']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
	

		<!-- ==================================================== END BODY ==================================================== -->



		<!-- ==================================================== START FOOTER ==================================================== -->
			<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<!-- ==================================================== END FOOTER ==================================================== -->

		</div> <!-- end wrapper -->
	</body>
</html>
<?php }} ?>