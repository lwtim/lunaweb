<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 21:06:33
         compiled from "views\email-marketing.html" */ ?>
<?php /*%%SmartyHeaderCode:1316525c448bdfbd96-39345563%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd2e69736bc8dad8f42cc6d105a2dec549350d038' => 
    array (
      0 => 'views\\email-marketing.html',
      1 => 1382129951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1316525c448bdfbd96-39345563',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c448bebe312_46106949',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c448bebe312_46106949')) {function content_525c448bebe312_46106949($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->
	<div id="wrapper-body-01">
<!-- start content -->
		<div id="content">
			<h1>
				Email Marketing
			</h1>
	<!-- start chart  -->		
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
	<!-- end chart  -->		
			<p>
				Email marketing remains one of the most effective and affordable means of advertising. But it needs to be done strategically to get results. We help clients by designing great-looking email newsletters, creating relevant content, and measuring the results. We connect email marketing to the overall strategy in support of the primary business goals.
				<br /><br />
				Whether you already have an active email marketing program or you want to get it started, we have you covered from start to finish. <a href="/lets-talk.php">Contact us</a> to learn more.
			</p>
		</div>
<!-- end content -->
	</div>
<!-- end wrapper-body-01  -->	
	<div class="border-full-01">&nbsp;</div><br /><br />

<!-- ==================================================== END BODY ==================================================== -->





<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">
		
<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>