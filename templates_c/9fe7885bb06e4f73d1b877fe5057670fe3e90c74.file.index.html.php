<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 16:08:02
         compiled from "views\index.html" */ ?>
<?php /*%%SmartyHeaderCode:103015252cde35816a2-21972955%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9fe7885bb06e4f73d1b877fe5057670fe3e90c74' => 
    array (
      0 => 'views\\index.html',
      1 => 1382129951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '103015252cde35816a2-21972955',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5252cde35832e2_41514193',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5252cde35832e2_41514193')) {function content_5252cde35832e2_41514193($_smarty_tpl) {?><!-- ==================================================== WELCOME ==================================================== -->
<!-- start wrapper-welcome-01  -->
	<div id="wrapper-welcome-01">
		<h1>
			Grow Your Business Online
		</h1>
		
	<!-- Download Guide  -->				
		<?php echo $_smarty_tpl->getSubTemplate ('widget_download_guide.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
		<img src="images/marketing-chart-03.png" alt="Memphis web marketing company - analytics, better ROI, SEO" />
	
		<p>
		If you own, run, or market a business, you face numerous challenges &mdash; acquire customers, grow sales, show a return on investment (ROI), manage marketing, get results, and maximize time. In addition to these demands, you must maintain your website &mdash; a crucial part of the picture. We can help simplify your life by working with you to develop an effective online marketing strategy and help you manage the complexity of a digital marketing world. Take a look around our site and <a href="/lets-talk.php">get in touch</a>.		</p>
		
</div>
<!-- end wrapper-welcome-01  -->	
	<div class="border-full-01">&nbsp;</div>

<!-- ==================================================== END WELCOME ==================================================== -->







<!-- ==================================================== SERVICES ==================================================== -->
<!-- start wrapper-services-01  -->
	<div id="wrapper-services-01">

<!-- start row  -->		
		<div class="row">
<!-- box-01  -->
			<div class="box">
				<a href="/memphis-inbound-marketing.php"><img src="design/icon-services-inbound.jpg" alt="memphis inbound marketnig" class="icon" /></a>
				
				<div class="copy">
					<h2>Inbound Marketing</h2>
					<p>Attract new visitors and convert them to customers. Increase sales.</p>
				</div>
			</div>
<!-- box-02  -->			
			<div class="box">
				<a href="/web-strategy-memphis.php"><img src="design/icon-services-marketing-strategy.jpg" alt="memphi marketing strategy" class="icon" /></a>
				
				<div class="copy">				
					<h2>Marketing Strategy</h2>
					<p>Goals, personas, brand, and tactics must line up to make maximum impact. Aim, then fire.</p>
				</div>
			</div>
<!-- box-03  -->
			<div class="box">
				<a href="/memphis-seo.php"><img src="design/icon-services-seo.jpg" alt="memphis search engine optimization" class="icon" /></a>
				
				<div class="copy">				
					<h2>Search Engine Optimization</h2>
					<p>Getting found in search engines is no accident. Rank high.</p>
				</div>
			</div>
		</div>
<!-- end row  -->	


		
<!-- start row  -->		
		<div class="row">
<!-- box-01  -->
			<div class="box">
				<img src="design/icon-services-lead-conversion.jpg" alt="memphis lead conversion" class="icon" />
				
				<div class="copy">
					<h2>Lead Conversion</h2>
					<p>Turn visitors into leads through great offers. Grow sales.</p>
				</div>
			</div>
<!-- box-02  -->			
			<div class="box">
				<a href="/website-design-development.php"><img src="design/icon-services-website-design.jpg" alt="memphis website design" class="icon" /></a>
				
				<div class="copy">				
					<h2>Website Design</h2>
					<p>Form and function work together for a great look and meaningful results. Design well.</p>
				</div>
			</div>
<!-- box-03  -->
			<div class="box">
				<a href="/website-analytics.php"><img src="design/icon-services-analytics.jpg" alt="memphis analytics" class="icon" /></a>
				
				<div class="copy">				
					<h2>Analytics</h2>
					<p>Numbers and data tell a story. Measure results.</p>
				</div>
			</div>
		</div>
<!-- end row  -->	

	
	</div>
<!-- end wrapper-services-01  -->		
	<br /><br />
	
	<div class="border-full-01">&nbsp;</div>

<!-- ==================================================== END SERVICES ==================================================== -->






<!-- ==================================================== TUBE ==================================================== -->
<!-- start wrapper-tube-01  -->
	<div id="wrapper-tube-01">
		<h2>Why LunaWeb?</h2>
		<p>Over the past 18 years, we have helped companies of all sizes use the Web to strategically grow their businesses, maximize their profits, and take better care of their customers. We have done this by designing and developing great websites, building and implementing targeted marketing strategies, and providing ongoing Web support including website hosting.</p><br />
Here's a short list of why businesses choose LunaWeb and have stayed with us for more than eight years on average:
<br /><br />
We get results.<br />
</p>
<!-- menu  -->			
			<div class="menu">
	
				<ul class="space-home">                                     
					
					<li>Customer service is essential; it's not just a clich&egrave;.</li>
					<li>We tailor everything to your goals, needs, and budget.</li>
					
					<li>LunaWeb was founded in 1995, and is the most experienced Web company in Memphis.</li>
					<li>Our staff&nbsp;has more than&nbsp;60 years combined experience in helping companies grow their business.</li>
				</ul> <br />
<br />

		<p><a href="/lets-talk.php">Get in touch</a> and see what LunaWeb can do for you.</p>		
	  </div>

</div>
<!-- end wrapper-tube-01  -->	
	<br /><br />
	
	<div class="border-full-01">&nbsp;</div>
	
	<br /><br />

<!-- ==================================================== END TUBE ==================================================== -->






<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
	<div id="widgets">

<!-- box 01  -->	
	<?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 02  -->			
	<?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!-- box 03  -->
	<?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
<!-- end widgets  -->		
	<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== --><?php }} ?>