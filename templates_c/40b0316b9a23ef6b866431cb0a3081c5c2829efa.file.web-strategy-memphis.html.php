<?php /* Smarty version Smarty-3.1.13, created on 2013-10-18 19:18:56
         compiled from "views\web-strategy-memphis.html" */ ?>
<?php /*%%SmartyHeaderCode:13308525c2369a5add9-63401925%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '40b0316b9a23ef6b866431cb0a3081c5c2829efa' => 
    array (
      0 => 'views\\web-strategy-memphis.html',
      1 => 1382129954,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13308525c2369a5add9-63401925',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_525c2369b1d4f5_38898720',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c2369b1d4f5_38898720')) {function content_525c2369b1d4f5_38898720($_smarty_tpl) {?><!-- ==================================================== BODY ==================================================== -->
<!-- start wrapper-body-01  -->

<div id="wrapper-body-01">
  <!-- start content -->
  <div id="content">
    <h1>Website Strategy Development</h1>
    <!-- start chart  -->
			<div id="chart-marketing-01"> <span class="title">6 Steps Download Free Guide:</span><br />
				
				<br />6 Steps to a Website That Gets Results<br><br>
				<span class="copy">Get the free guide <a href="http://offers.lunaweb.com/6-steps-to-a-website-that-gets-results/" target="_blank">HERE</a><br>		
				<span class="list"> design<br />coding <br />marketing<br /></span> 
					  
			</div>
    <!-- end chart  -->
    

    <p>Every good marketing move starts with a strategy, and that strategy starts with your goals. No matter how high you're aiming, we've got the tools and know-how you need. Call to tell us where you'd like to be in a year &mdash; we'll help you build the right web strategy to get you there. </p><br />


<p>Most people agree that a strategy is important but not everyone has an active strategy in place. That's where we help. We work with business owners, leaders, and marketers to build effective web strategies. A solid online strategy includes the following clearly defined elements:</p><br />
<br />

<ul>
<li>Goals that are specific and measurable (KPIs)</li>
<li>Descriptions of your target audiences (Personas)</li>
<li>Value proposition and brand difference</li>
<li>Analysis of competitors</li>
<li>Industry overview</li>
<li>Key messages</li>
<li>Marketing tactics and channels</li>
</ul>
  </div>
  <!-- end content -->
</div>
<!-- end wrapper-body-01  -->
<div class="border-full-01">&nbsp;</div>
<br />
<br />
<!-- ==================================================== END BODY ==================================================== -->
<!-- ==================================================== CTA ==================================================== -->
<!-- start widgets  -->
<div id="widgets">
  <!-- box 01  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_newsletter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  <!-- box 02  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_expedition.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  <!-- box 03  -->
  <?php echo $_smarty_tpl->getSubTemplate ('widget_facebook.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<!-- end widgets  -->
<div class="border-full-01">&nbsp;</div>
<!-- ==================================================== END CTA ==================================================== -->
<?php }} ?>