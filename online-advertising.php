<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Online Advertising - Memphis, Nationwide | Google ads, ppc marketing");
	$smarty->assign("description","Online advertising experts in Memphis. Let us assist you with ppc ads, email marketing, blog creation and consulting, social media, seo, etc.");
	$smarty->assign("keywords","online advertising memphis, ppc marketing, ppc ads, google ads management memphis, online marketing");
	$smarty->view();
?>