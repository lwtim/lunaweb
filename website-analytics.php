<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Website analytics | Website optimization - Memphis");
	$smarty->assign("description","Website analytics, website optimization and landing page optimization by Memphis' oldest local website design firm, LunaWeb.");
	$smarty->assign("keywords","website analytics memphis, website optimization memphis, web design memphis, social media memphis, seo memphis, mobile marketing memphis");
	$smarty->view();
?>