<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Memphis SEO | Internet marketing  | Website design - LunaWeb, Inc.");
	$smarty->assign("description","Local Memphis SEO, internet marketing services and national SEO. Call today before your competitors do - website design, website optimization, pay-per-click ads (ppc) and social media. ");
	$smarty->assign("keywords","memphis seo, internet marketing, webstie design, web design Memphis, internet marketing memphis, seo memphis, web designer memphis, social media memphis, inbound marketing memphis");
	$smarty->view();
?>