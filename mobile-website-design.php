<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Mobile website design and development in Memphis - LunaWeb");
	$smarty->assign("description","Mobile website design by Memphis' oldest local web design company. Offering SEO & inbound marketing expertise, social media and more - call now!");
	$smarty->assign("keywords","mobile website design memphis, web design memphis, seo memphis, ecommerce web design memphis, website company in memphis tennessee");
	$smarty->view();
?>