<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("page_not_found","true");
	
	$smarty->assign("title","404 Page Not Found");
	$smarty->assign("description","");
	$smarty->assign("keywords","");
	$smarty->assign("viewFile","views/sitemap.html");
	$smarty->display("long_content.tpl");
?>