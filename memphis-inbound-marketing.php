<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Memphis inbound marketing company - LunaWeb, Inc.");
	$smarty->assign("description","Need help with inbound marketing? Memphis specialists in web marketing strategies, including web optimization, analytics, email marketing, etc.");
	$smarty->assign("keywords","memphis inbound marketing, web marketing memphis, internet marketing memphis, internet strategy development, memphis seo, PPC ads");
	$smarty->view();
?>