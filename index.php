<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("homepage","true");
	
	$smarty->assign("title","Web design Memphis | Internet Marketing & SEO - LunaWeb, Inc.");
	$smarty->assign("description","Web design and internet marketing company in Memphis. Social media consulting, website marketing, SEO and more. Partner today with LunaWeb - since 1995.");
	$smarty->assign("keywords","web design Memphis, internet marketing memphis, seo memphis, web designer memphis, social media memphis, inbound marketing memphis");
	$smarty->view();
?>