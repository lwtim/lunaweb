<?php
	require_once("libs/Smarty.construct.php");
	
	$smarty->assign("title","Email marketing - Memphis web marketing company: LunaWeb");
	$smarty->assign("description","Email marketing services in Memphis. Web marketing company, LunaWeb, will show you how to send, manage and track email newsletters and campaigns.");
	$smarty->assign("keywords","email marketing memphis, ppc advertising memphis, internet advertising memphis, online marketing memphis");
	$smarty->view();
?>